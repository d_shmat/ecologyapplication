package com.example.dmitry.ecologyapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class ScanActivity extends Activity {

    SurfaceView cameraView;
    BarcodeDetector barcode;
    CameraSource cameraSource;
    SurfaceHolder holder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        cameraView = findViewById(R.id.cameraView);
        cameraView.setZOrderMediaOverlay(true);
        holder = cameraView.getHolder();
        barcode = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        setupScanner();
    }

    private void setupScanner() {
        if (!barcode.isOperational()) {
            Toast.makeText(getApplicationContext(), "Sorry, Couldn't setup the detector", Toast.LENGTH_LONG).show();
            this.finish();
        }
        cameraSource = new CameraSource.Builder(this, barcode)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(24)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1920, 1024)
                .build();
        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(cameraView.getHolder());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
        barcode.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() > 0) {
                    Barcode barcode = barcodes.valueAt(0);
                    switch (barcode.displayValue) {
                        case "20":
                            Intent reward20Intent = new Intent(ScanActivity.this, SaveCityActivity.class);
                            reward20Intent.putExtra("background", R.drawable.reward_20);
                            startActivity(reward20Intent);
                            finish();
                            break;
                        case "30":
                            Intent reward30Intent = new Intent(ScanActivity.this, SaveCityActivity.class);
                            reward30Intent.putExtra("background", R.drawable.reward_30);
                            startActivity(reward30Intent);
                            finish();
                            break;
                        case "40":
                            Intent reward40Intent = new Intent(ScanActivity.this, SaveCityActivity.class);
                            reward40Intent.putExtra("background", R.drawable.reward_40);
                            startActivity(reward40Intent);
                            finish();
                            break;
                        case "50":
                            Intent reward50Intent = new Intent(ScanActivity.this, SaveCityActivity.class);
                            reward50Intent.putExtra("background", R.drawable.reward);
                            startActivity(reward50Intent);
                            finish();
                            break;
                    }
                }
            }
        });
    }


    private void releaseCamera() {
        cameraSource.stop();
        cameraSource.release();
        barcode.release();
    }

    @Override
    protected void onDestroy() {
        if (cameraSource != null) {
            releaseCamera();
        }
        super.onDestroy();
    }
}
