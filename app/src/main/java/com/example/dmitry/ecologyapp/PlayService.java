package com.example.dmitry.ecologyapp;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class PlayService extends Service {
    private MediaPlayer mPlayer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        /*Toast.makeText(this, "Служба создана",
                Toast.LENGTH_SHORT).show();*/
        mPlayer = MediaPlayer.create(this, R.raw.main_music);
        mPlayer.setLooping(true);
    }

    // Устаревший метод
//    @Override
//    public void onStart(Intent intent, int startId) {
//        super.onStart(intent, startId);
//        Toast.makeText(this, "Служба запущена",
//                Toast.LENGTH_SHORT).show();
//        mPlayer.start();
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       /* Toast.makeText(this, "Служба запущена",
                Toast.LENGTH_SHORT).show();*/
        if(intent.getIntExtra("status", -1) == 0){
            mPlayer.start();
        }else if(intent.getIntExtra("status", -1) == 1){
            mPlayer.pause();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       /* Toast.makeText(this, "Служба остановлена",
                Toast.LENGTH_SHORT).show();*/
        mPlayer.pause();
    }
}
