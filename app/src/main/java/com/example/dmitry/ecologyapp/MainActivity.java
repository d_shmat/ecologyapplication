package com.example.dmitry.ecologyapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final int PERMISSION_REQUEST = 200;
    ImageView musicImage;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setHomeAsUpIndicator(R.drawable.menu);

        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
        actionbar.setCustomView(R.layout.action_bar);
        actionbar.setHomeButtonEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);

        ((TextView) findViewById(R.id.profile_ab_text)).setText(getString(R.string.profile));

        findViewById(R.id.save_city_button).setOnClickListener(this);
        findViewById(R.id.eco_points_button).setOnClickListener(this);
        findViewById(R.id.rewards_button).setOnClickListener(this);
        findViewById(R.id.memo_button).setOnClickListener(this);

        findViewById(R.id.profile_ab_image).setOnClickListener(this);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        findViewById(R.id.nav_save).setOnClickListener(this);
        findViewById(R.id.nav_eco_tips).setOnClickListener(this);
        findViewById(R.id.nav_rewards).setOnClickListener(this);
        findViewById(R.id.nav_memo).setOnClickListener(this);
        findViewById(R.id.nav_rate).setOnClickListener(this);

        musicImage = (ImageView) findViewById(R.id.music_image);
        musicImage.setOnClickListener(this);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
        }

        if (!DataClass.isMusicPlaying) {
            musicImage.setImageResource(R.drawable.music_off);
        } else {
            musicImage.setImageResource(R.drawable.music_on);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else
                    mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.nav_save:
            case R.id.save_city_button:
                startActivity(new Intent(MainActivity.this, ScanActivity.class));
                break;
            case R.id.eco_points_button:
            case R.id.nav_eco_tips:
                intent = new Intent(this, MapActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_rewards:
            case R.id.rewards_button:
                intent = new Intent(this, CouponsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_memo:
            case R.id.memo_button:
                intent = new Intent(this, MemoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_rate:
                intent = new Intent(this, RatingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.music_image:
                if (!DataClass.isMusicPlaying) {
                    startService(new Intent(MainActivity.this, PlayService.class).putExtra("status", 0));
                    musicImage.setImageResource(R.drawable.music_on);
                    DataClass.isMusicPlaying = !DataClass.isMusicPlaying;
                } else {
                    startService(new Intent(MainActivity.this, PlayService.class).putExtra("status", 1));
                    musicImage.setImageResource(R.drawable.music_off);
                    DataClass.isMusicPlaying = !DataClass.isMusicPlaying;
                }
                break;
        }

    }
}

