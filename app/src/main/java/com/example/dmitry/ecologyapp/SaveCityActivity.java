package com.example.dmitry.ecologyapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class SaveCityActivity extends Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.save_city_activity);

        Button exitButton = findViewById(R.id.reward_close_button);
        exitButton.setOnClickListener(this);
        exitButton.setTypeface(Typeface.createFromAsset(getAssets(), "baronneueblack.ttf"));

        int imageId = getIntent().getIntExtra("background", 0);

        findViewById(R.id.drawer_layout).setBackground(getResources().getDrawable(imageId));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reward_close_button:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}

