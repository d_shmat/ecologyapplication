package com.example.dmitry.ecologyapp;

public class MarkerWithAddress {
    private double latitude;
    private double longitude;
    private String addresss;

    public MarkerWithAddress(double lat, double lon, String address){
        this.latitude = lat;
        this.longitude = lon;
        this.addresss = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddresss() {
        return addresss;
    }
}
