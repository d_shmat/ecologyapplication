package com.example.dmitry.ecologyapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.VideoView;

public class VideoActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);

        int imageId = getIntent().getIntExtra("videoName", 0);
        VideoView view = (VideoView) findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + imageId;
        view.setVideoURI(Uri.parse(path));
        view.start();
    }
}
