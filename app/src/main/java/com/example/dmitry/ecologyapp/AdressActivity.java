package com.example.dmitry.ecologyapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

public class AdressActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_adress_activity);
        this.setFinishOnTouchOutside(true);

        String addressIntent = getIntent().getStringExtra("addressIntent");
        TextView tw = (TextView) findViewById(R.id.text_address);
        tw.setText(addressIntent);
    }
}
