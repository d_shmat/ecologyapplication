package com.example.dmitry.ecologyapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private DrawerLayout mDrawerLayout;
    private ArrayList<Marker> paperListMarker;
    private ArrayList<Marker> plastickListMarker;
    private ArrayList<Marker> glassListMarker;
    private ArrayList<Marker> ironListMarker;
    private boolean paperFlag;
    private boolean plastickFlag;
    private boolean glassFlag;
    private boolean ironFlag;
    private Button paperButton;
    private Button plasticButton;
    private Button glassButton;
    private Button ironButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        paperFlag = false;
        plastickFlag = false;
        glassFlag = false;
        ironFlag = false;

        ActionBar actionbar = getSupportActionBar();
        actionbar.setHomeAsUpIndicator(R.drawable.menu);


        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
        actionbar.setCustomView(R.layout.action_bar);
        actionbar.setHomeButtonEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);

        ((TextView) findViewById(R.id.profile_ab_text)).setText(getString(R.string.map_action_bar));
        findViewById(R.id.profile_ab_image).setOnClickListener(this);

        mDrawerLayout = findViewById(R.id.drawer_layout);


        findViewById(R.id.nav_save).setOnClickListener(this);
        findViewById(R.id.nav_eco_tips).setOnClickListener(this);
        findViewById(R.id.nav_rewards).setOnClickListener(this);
        findViewById(R.id.nav_memo).setOnClickListener(this);
        findViewById(R.id.nav_rate).setOnClickListener(this);


        paperButton = findViewById(R.id.paperButton);
        paperButton.setOnClickListener(this);
        plasticButton = findViewById(R.id.plastickButton);
        plasticButton.setOnClickListener(this);
        glassButton = findViewById(R.id.glassButton);
        glassButton.setOnClickListener(this);
        ironButton = findViewById(R.id.ironButton);
        ironButton.setOnClickListener(this);

        paperListMarker = new ArrayList<>();
        plastickListMarker = new ArrayList<>();
        glassListMarker = new ArrayList<>();
        ironListMarker = new ArrayList<>();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);


        // Add a marker in Sydney and move the camera
        LatLng piter = new LatLng(59.91, 30.34);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(piter)      // Sets the center of the map to Mountain View
                .zoom(10)                      // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        paperListMarker.clear();
        plastickListMarker.clear();
        glassListMarker.clear();
        ironListMarker.clear();

        paperListMarker = addPaperMarkers();
        plastickListMarker = addPlasticMarkers();
        glassListMarker = addGlassMarkers();
        ironListMarker = addIronMarkers();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.profile_ab_image:
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.paperButton:
                showMarkers(paperListMarker, paperFlag);
                paperFlag = !paperFlag;
                if (paperFlag)
                    paperButton.setBackground(getResources().getDrawable(R.drawable.paper_button_active));
                else
                    paperButton.setBackground(getResources().getDrawable(R.drawable.paper_button));
                break;
            case R.id.plastickButton:
                showMarkers(plastickListMarker, plastickFlag);
                plastickFlag = !plastickFlag;
                if (plastickFlag)
                    plasticButton.setBackground(getResources().getDrawable(R.drawable.plastick_button_active));
                else
                    plasticButton.setBackground(getResources().getDrawable(R.drawable.plastick_button));
                break;
            case R.id.glassButton:
                showMarkers(glassListMarker, glassFlag);
                glassFlag = !glassFlag;
                if (glassFlag)
                    glassButton.setBackground(getResources().getDrawable(R.drawable.glass_button_active));
                else
                    glassButton.setBackground(getResources().getDrawable(R.drawable.glass_button));
                break;
            case R.id.ironButton:
                showMarkers(ironListMarker, ironFlag);
                ironFlag = !ironFlag;
                if (ironFlag)
                    ironButton.setBackground(getResources().getDrawable(R.drawable.iron_button_active));
                else
                    ironButton.setBackground(getResources().getDrawable(R.drawable.iron_button));
                break;

            case R.id.nav_save:
                startActivity(new Intent(MapActivity.this, ScanActivity.class));
                break;
            case R.id.nav_eco_tips:
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_rewards:
                intent = new Intent(this, CouponsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_memo:
                intent = new Intent(this, MemoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_rate:
                intent = new Intent(this, RatingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else
                    mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Marker> addPaperMarkers() {
        ArrayList<Marker> markers = new ArrayList<>();
        MarkerWithAddress[] markersWithAddresses = new MarkerWithAddress[]{
                new MarkerWithAddress(59.926657, 30.4712892, "российский пр., 8,"),
                new MarkerWithAddress(59.9145407, 30.51569270, "ленинградская ул., 7, Кудрово"),
                new MarkerWithAddress(59.91945930000001, 30.499765300000035, "подвойского ул. , 48к1"),
                new MarkerWithAddress(59.9538248, 30.4233016, "металистов ул, 19/30"),
                new MarkerWithAddress(59.9623417, 30.4122932, "полюстровский пр., 3"),
                new MarkerWithAddress(59.96588029999999, 30.37143489999994, "ул. минеральная, 13, лит. З"),
                new MarkerWithAddress(59.962269, 30.279060, "пионерская ул., 63"),
                new MarkerWithAddress(59.95050699999999, 30.24673210000003, "просп. КИМа, 6"),
                new MarkerWithAddress(59.9113563, 30.3015424, "12-я красноармейская ул., 16"),
                new MarkerWithAddress(59.8930745, 30.3249628, "лиговский пр., 270"),
        };
        for (MarkerWithAddress markerWithAddress : markersWithAddresses) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(
                    new LatLng(markerWithAddress.getLatitude(), markerWithAddress.getLongitude()))
                    .visible(false)
                    .title(markerWithAddress.getAddresss())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker)));
            markers.add(marker);
        }
        return markers;
    }

    private ArrayList<Marker> addPlasticMarkers() {
        ArrayList<Marker> markers = new ArrayList<>();
        MarkerWithAddress[] markersWithAddresses = new MarkerWithAddress[]{
                new MarkerWithAddress(60.030427, 30.400921, "пр. луначарского, 96к2"),
                new MarkerWithAddress(60.014396, 30.452578, "пискарёвский пр., 150, корп. 2"),
                new MarkerWithAddress(59.982923, 30.392857, " 64 к.4, Кондратьевский пр."),
                new MarkerWithAddress(59.9488877, 30.2523940, "наб. р. смоленки, 35к1"),
                new MarkerWithAddress(59.893078, 30.259503, "оборонная ул., 22"),
                new MarkerWithAddress(59.8762548, 30.3160391, "благодатная ул., 20"),
                new MarkerWithAddress(59.8695233, 30.4111129, "софийская ул., 44-а"),
                new MarkerWithAddress(59.858318, 30.347452, "пр. космонавтов, 29 корпус 2"),
                new MarkerWithAddress(59.873373, 30.264362, "краснопутиловская ул., 12"),
                new MarkerWithAddress(59.864611, 30.463412, "ново-александровская ул., 14"),
        };
        for (MarkerWithAddress markerWithAddress : markersWithAddresses) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(
                    new LatLng(markerWithAddress.getLatitude(), markerWithAddress.getLongitude()))
                    .visible(false)
                    .title(markerWithAddress.getAddresss())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_marker)));
            markers.add(marker);
        }
        return markers;
    }

    private ArrayList<Marker> addGlassMarkers() {
        ArrayList<Marker> markers = new ArrayList<>();
        MarkerWithAddress[] markersWithAddresses = new MarkerWithAddress[]{
                new MarkerWithAddress(59.887496, 30.3208640, "московский пр., 136, к. 2А"),
                new MarkerWithAddress(59.9873223, 30.3716455, "кушелевская дор., 3к7"),
                new MarkerWithAddress(60.0077730, 30.3987409, "ул. верности, 9"),
                new MarkerWithAddress(60.0131269, 30.4284850, "ул. карпинского, 38, корп. 1"),
                new MarkerWithAddress(59.962269, 30.279060, "пионерская ул., 63"),
                new MarkerWithAddress(59.9748465, 30.316947, "пр. медиков, 10"),
                new MarkerWithAddress(59.979293, 30.380796, "пр. маршала блюхера, 11"),
                new MarkerWithAddress(60.0117050, 30.2437069, "долгоозeрная ул., 5, корп. 1"),
                new MarkerWithAddress(60.0308298, 30.2439764, "пр. королёва, 61"),
                new MarkerWithAddress(60.055478, 30.338519, "пр. энгельса, 145"),
        };
        for (MarkerWithAddress markerWithAddress : markersWithAddresses) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(
                    new LatLng(markerWithAddress.getLatitude(), markerWithAddress.getLongitude()))
                    .visible(false)
                    .title(markerWithAddress.getAddresss())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_marker)));
            markers.add(marker);
        }
        return markers;
    }

    private ArrayList<Marker> addIronMarkers() {
        ArrayList<Marker> markers = new ArrayList<>();
        MarkerWithAddress[] markersWithAddresses = new MarkerWithAddress[]{
                new MarkerWithAddress(59.899803, 30.493663, "ул. крыленко, 43, корп. 2"),
                new MarkerWithAddress(59.9148954, 30.4475816, "ул. коллонтай, 4 корпус 1"),
                new MarkerWithAddress(59.926275, 30.425257, "казанская ул. (малая охта), 8"),
                new MarkerWithAddress(59.9470796, 30.3797932, "тверская ул., 6"),
                new MarkerWithAddress(59.95244, 30.41724, "панфилова ул., 26"),
                new MarkerWithAddress(59.9658249, 30.3662301, "арсенальная ул., 70"),
                new MarkerWithAddress(59.961138, 30.286369, "большая разночинная ул., 28,"),
                new MarkerWithAddress(59.962269, 30.279060, "пионерская ул., 63\n"),
                new MarkerWithAddress(59.9904458, 30.3181422, "сердобольская ул., 27"),
                new MarkerWithAddress(60.002854, 30.360443, " пр. тореза, 21"),
        };
        for (MarkerWithAddress markerWithAddress : markersWithAddresses) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(
                    new LatLng(markerWithAddress.getLatitude(), markerWithAddress.getLongitude()))
                    .visible(false)
                    .title(markerWithAddress.getAddresss())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.red_marker)));
            markers.add(marker);
        }
        return markers;
    }

    private void showMarkers(ArrayList<Marker> markersArray, boolean flag) {
        if (flag)
            for (int i = 0; i < markersArray.size(); i++) {
                markersArray.get(i).setVisible(false);
            }
        else
            for (int i = 0; i < markersArray.size(); i++) {
                markersArray.get(i).setVisible(true);
            }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        showDialog(this, marker.getTitle());
        marker.hideInfoWindow();
        return true;
    }

    public void showDialog(Activity activity, String msg) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.map_adress_activity);
        dialog.getWindow().setBackgroundDrawableResource(R.color.tranparentColor);

        TextView text = (TextView) dialog.findViewById(R.id.text_address);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}