package com.example.dmitry.ecologyapp;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

public class SemiCouponActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.semi_coupon_activity);
        this.setFinishOnTouchOutside(true);


        int imageId = getIntent().getIntExtra("couponImage", 0);

        ((ImageView) findViewById(R.id.semi_coupon_image_view)).setImageResource(imageId);
        ((ImageView) findViewById(R.id.close_coupon_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
