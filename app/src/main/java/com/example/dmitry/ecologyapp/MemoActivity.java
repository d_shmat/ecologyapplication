package com.example.dmitry.ecologyapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

public class MemoActivity extends AppCompatActivity implements View.OnClickListener {

    private DrawerLayout mDrawerLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memo_activity);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setHomeAsUpIndicator(R.drawable.menu);


        actionbar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
        actionbar.setCustomView(R.layout.action_bar);
        actionbar.setHomeButtonEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.profile_ab_image).setOnClickListener(this);

        ((TextView) findViewById(R.id.profile_ab_text)).setText(getString(R.string.memo_action_bar));

        mDrawerLayout = findViewById(R.id.drawer_layout);


        findViewById(R.id.nav_save).setOnClickListener(this);
        findViewById(R.id.nav_eco_tips).setOnClickListener(this);
        findViewById(R.id.nav_rewards).setOnClickListener(this);
        findViewById(R.id.nav_memo).setOnClickListener(this);
        findViewById(R.id.nav_rate).setOnClickListener(this);

        View glassImg = findViewById(R.id.memo_glass_image);
        glassImg.setOnClickListener(this);

        findViewById(R.id.memo_paper_image).setOnClickListener(this);
        findViewById(R.id.memo_iron_image).setOnClickListener(this);
        findViewById(R.id.memo_plastick_image).setOnClickListener(this);
        findViewById(R.id.memo_pdf_image).setOnClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else
                    mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.profile_ab_image:
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.memo_glass_image:
                intent = new Intent(MemoActivity.this, VideoActivity.class);
                intent.putExtra("videoName", R.raw.steklo);
                if (DataClass.isMusicPlaying) {
                    startService(new Intent(this, PlayService.class).putExtra("status", 1));
                    DataClass.isMemoPause = true;
                }
                startActivity(intent);
                break;
            case R.id.memo_paper_image:
                intent = new Intent(MemoActivity.this, VideoActivity.class);
                intent.putExtra("videoName", R.raw.paper);
                if (DataClass.isMusicPlaying) {
                    startService(new Intent(this, PlayService.class).putExtra("status", 1));
                    DataClass.isMemoPause = true;
                }
                startActivity(intent);
                break;
            case R.id.memo_iron_image:
                intent = new Intent(MemoActivity.this, VideoActivity.class);
                intent.putExtra("videoName", R.raw.metall);
                if (DataClass.isMusicPlaying) {
                    startService(new Intent(this, PlayService.class).putExtra("status", 1));
                    DataClass.isMemoPause = true;
                }
                startActivity(intent);
                break;
            case R.id.memo_plastick_image:
                intent = new Intent(MemoActivity.this, VideoActivity.class);
                intent.putExtra("videoName", R.raw.plastik);
                if (DataClass.isMusicPlaying) {
                    startService(new Intent(this, PlayService.class).putExtra("status", 1));
                    DataClass.isMemoPause = true;
                }
                startActivity(intent);
                break;
            case R.id.memo_pdf_image:
                try {
                    if (Build.VERSION.SDK_INT >= 24) {
                        try {
                            Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                            m.invoke(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    File fileBrochure = new File(Environment.getExternalStorageDirectory() + "/" + "rules.pdf");
                    if (!fileBrochure.exists()) {
                        CopyAssetsbrochure();
                    }

                    File file = new File(Environment.getExternalStorageDirectory() + "/" + "rules.pdf");
                    Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
                    pdfIntent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    pdfIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        getApplicationContext().startActivity(pdfIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(MemoActivity.this, "NO Pdf Viewer", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    new AlertDialog.Builder(this).setMessage(e.toString()).show();
                }
                break;

            case R.id.nav_save:
                startActivity(new Intent(this, ScanActivity.class));
                finish();
                break;
            case R.id.nav_eco_tips:
                intent = new Intent(this, MapActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_rewards:
                intent = new Intent(this, CouponsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.nav_memo:
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_rate:
                intent = new Intent(this, RatingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void CopyAssetsbrochure() {
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        for (int i = 0; i < files.length; i++) {
            String fStr = files[i];
            if (fStr.equalsIgnoreCase("rules.pdf")) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(files[i]);
                    out = new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + files[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                    break;
                } catch (Exception e) {
                    Log.e("tag", e.getMessage());
                }
            }
        }
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (DataClass.isMemoPause)
            startService(new Intent(this, PlayService.class).putExtra("status", 0));
        DataClass.isMemoPause = false;
    }
}